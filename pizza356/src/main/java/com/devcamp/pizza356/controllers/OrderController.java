package com.devcamp.pizza356.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CProduct;
import com.devcamp.pizza356.services.OrderService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)

public class OrderController {
    @Autowired
    OrderService orderService;

    
    // lấy danh sách product truyen order code
    @GetMapping("/products")
    public ResponseEntity<Set<CProduct>> getProductByOrderCode(@RequestParam(value = "orderId") long orderId) {
        try {
            Set<CProduct> products = orderService.getProductByOrderID(orderId);
            if (products != null) {
                return new ResponseEntity<>(products, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
