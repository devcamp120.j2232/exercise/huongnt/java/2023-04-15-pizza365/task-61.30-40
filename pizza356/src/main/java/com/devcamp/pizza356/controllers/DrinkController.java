package com.devcamp.pizza356.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza356.models.CDrink;
import com.devcamp.pizza356.services.DrinkService;



@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired DrinkService drinkService;
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinkAPI(){
        try {
			return new ResponseEntity<>(drinkService.getAllDrinks(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}
