package com.devcamp.pizza356.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza356.models.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher,Long>{
    
}
