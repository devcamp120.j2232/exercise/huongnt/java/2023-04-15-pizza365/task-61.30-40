package com.devcamp.pizza356.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza356.models.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu,Long>{
    
}
