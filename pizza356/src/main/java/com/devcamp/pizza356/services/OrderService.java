package com.devcamp.pizza356.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.COrder;
import com.devcamp.pizza356.models.CProduct;
import com.devcamp.pizza356.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository pOrderRepository;

    public ArrayList<COrder> getAllOrderList() {
    ArrayList<COrder> orderList = new ArrayList<>();
        
    List<COrder> pOrder = new ArrayList<COrder>();

    pOrderRepository.findAll().forEach(pOrder::add);
    return orderList;
}

public Set<CProduct> getProductByOrderID(long id) { 
    COrder vOrder = pOrderRepository.findById(id);
    if (vOrder != null) {
        return  vOrder.getProducts();
    } else {
        return null;
    }
}


}
