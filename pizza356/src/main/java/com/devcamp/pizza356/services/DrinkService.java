package com.devcamp.pizza356.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza356.models.CDrink;
import com.devcamp.pizza356.repository.IDrinkRepository;

@Service
public class DrinkService {
    @Autowired
    IDrinkRepository pDrinkRepository;
    public ArrayList<CDrink> getAllDrinks() {
        ArrayList<CDrink> drinkList = new ArrayList<>();
        pDrinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }
    
}
